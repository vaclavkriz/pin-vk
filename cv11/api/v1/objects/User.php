<?php 
require_once('../helpers/Guid.php');

class User {
    private $conn; 
    private $table_name = "users";
    public $id;
    public $firstname;
    public $lastname;
    public $number;
    
    public function __construct($db) {
        $this->conn = $db;
    }
    
    public function read(){
        $query = "SELECT * FROM `$this->table_name`;";
        $result = $this->conn->query($query);
        return $result;
    }
    public function create(){
        $this->id = GuidV::generate();
        $stmt = $this->conn->prepare("INSERT INTO `$this->table_name` VALUES (?, ?, ?, ?);");
        $stmt->bind_param("sssi", $this->id, $this->firstname, $this->lastname, $this->number);
        return $stmt->execute();    
    }
    public function update(){
        $stmt = $this->conn->prepare("UPDATE `$this->table_name` SET number = ?, firstname = ?, lastname = ? WHERE id = ?");
        $stmt->bind_param("isss", $this->number,$this->firstname, $this->lastname, $this->id);
        return $stmt->execute();    
    }
    public function delete(){
        $stmt = $this->conn->prepare("DELETE FROM `$this->table_name` WHERE id = ?");
        $stmt->bind_param("s", $this->id);
        return $stmt->execute();   
    }
}