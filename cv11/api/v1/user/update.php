<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
// instantiate product object
include_once '../objects/User.php';
 
$database = new Database();
$db = $database->getConnection();
 
$user = new User($db);
 
$data = json_decode(file_get_contents("php://input"));
if(!empty($data->id) && !empty($data->firstname) &&!empty($data->lastname) && $data->number >= 0){
    $user->id = $data->id;
    $user->number = $data->number;
    $user->firstname = $data->firstname;
    $user->lastname = $data->lastname;
 
 
    if($user->update()){
        http_response_code(201);
        echo json_encode(array("message" => "User was updated."));
    }
    else{
        http_response_code(503);
        echo json_encode(array("message" => "Unable to update user."));
    }
}
else{
    http_response_code(400);
    echo json_encode(array("message" => "Unable to update user. Data is incomplete."));
}
?>