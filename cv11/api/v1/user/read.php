<?php
// required headers

include_once '../config/database.php';
include_once '../objects/User.php';
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// instantiate database and product object
$database = new Database();


$db = $database->getConnection();
// initialize object
$user = new User($db);
$result = $user->read();
$resArray = array();
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        extract($row);
        $user = array("id" => $id,"firstname" => $firstname, "lastname" => $lastname, "number" => $number);
        array_push($resArray,$user);
    }

    http_response_code(200);
    echo json_encode($resArray);

} else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no products found
    echo json_encode(
        array()
    );
}