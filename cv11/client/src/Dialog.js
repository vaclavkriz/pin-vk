import React, { useState } from 'react';
import { addUser, changeUser } from './api';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import LinearProgress from '@material-ui/core/LinearProgress';


export default props => {
    //const [open, setOpen] = React.useState(false);
    const initInput = {
        firstName: '',
        lastName: '',
        number: ''
    }
    const [formData, setFormData] = useState({
        firstName: '',
        lastName: '',
        number: ''
    })
    const [errMsg, setErrMsg] = useState("");
    const [loading, setLoading] = useState(false);

    const formChange = (e) => {
        setFormData({ ...formData, [e.target.id]: e.target.value })
    }
    const validate = async e => {
        setLoading(true);
        if (Object.keys(formData).some(k => !formData[k])) {
            setLoading(false);
            setErrMsg("Fill the form again, please.");
        }
        else {

            setErrMsg("");
            props.close();
            if (!props.isUpdate) {
                const a = await createUser(formData);
            }
            else {
                const a = await updateUser({ id: props.preData.id, ...formData });
            }
            const b = await props.update();
        }
        setFormData({
            firstName: '',
            lastName: '',
            number: ''
        })
        setLoading(false);
        setFormData(initInput);
    }

    const createUser = async ({ firstName, lastName, number }) => {
        const res = await addUser(firstName, lastName, number);
        const data = await res.json();
        console.log(data)
    }
    const updateUser = async ({ id, firstName, lastName, number }) => {
        console.log(firstName);
        const res = await changeUser(id, firstName, lastName, number);
        const data = await res.json();
        console.log(data)
    }

    const handleClose = () => {
        // setOpen(false);
    };

    const handleOpen = () => {
        if (Object.keys(props.preData).length) {
            const { id, firstname, lastname, number } = props.preData;
            setFormData({
                firstName: firstname,
                lastName: lastname,
                number: number
            })
        }
    }

    return (
        <div>
            {/* <Button variant="outlined" color="primary" onClick={handleClickOpen}>
                Open form dialog
      </Button> */}
            <Dialog open={props.open} onEnter={handleOpen} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">{Object.keys(props.preData).length > 0 ? 'Edit user' : 'Add new user'}</DialogTitle>
                <DialogContent>
                    <form >
                        <TextField
                            autoFocus
                            margin="dense"
                            id="firstName"
                            label="First Name"
                            type="text"
                            fullWidth
                            value={formData.firstName}
                            required
                            onChange={formChange}
                        />
                        <TextField
                            margin="dense"
                            id="lastName"
                            label="Last Name"
                            type="text"
                            fullWidth
                            value={formData.lastName}
                            required
                            onChange={formChange}
                        />
                        <TextField
                            margin="dense"
                            id="number"
                            label="Number"
                            type="number"
                            fullWidth
                            value={formData.number}
                            required
                            onChange={formChange}
                        />
                        <span style={{ color: 'red' }}>{errMsg}</span>
                        {loading && <LinearProgress />}
                    </form>

                </DialogContent>
                <DialogActions>
                    <Button onClick={props.close} color="primary">
                        Zavřít
          </Button>
                    <Button onClick={validate} color="primary">
                        Přidat
          </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}