import React, { useState, useEffect } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';

import FormDialog from './Dialog';
import { getUsers, deleteUser } from './api';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import CircularProgress from '@material-ui/core/CircularProgress'



const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    minWidth: 300,
  },
});


const App = () => {
  const [users, setUsers] = useState([]);
  const [isOpen, setIsOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [preData, setPreData] = useState({});
  const [isUpdate, setIsUpdate] = useState(false);


  const classes = useStyles();
  const fetchData = async () => {
    setIsLoading(true);
    const res = await getUsers();
    const data = await res.json();
    setUsers(data);
    setIsLoading(false);
  }
  useEffect(() => {
    fetchData();
  }, []);

  const openPopup = () => {
    console.log('C')
    setIsUpdate(false);
    setIsOpen(true)
  }
  const close = () => {
    setIsOpen(false)
  }

  const update = () => {
    fetchData();
  }

  const deleteOne = async (id) => {
    setIsLoading(true);
    const res = await deleteUser(id);
    const json = await res.json();
    update();
    setIsLoading(false);
  }
  const editUser = data => {
    setPreData(data);
    setIsUpdate(true);
    setIsOpen(true);
  }

  return (
    <React.Fragment>
      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', marginTop: '10px' }}>
        {isLoading ? <CircularProgress /> : (


          <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', width: '50%' }}>
            <Paper className={classes.root} >
              <Table className={classes.table} aria-label="customized table">
                <TableHead>
                  <TableRow>
                    <StyledTableCell>First name</StyledTableCell>
                    <StyledTableCell>Last name</StyledTableCell>
                    <StyledTableCell >Number</StyledTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {users.map(row => (
                    <StyledTableRow key={row.id}>
                      <StyledTableCell> {row.firstname}</StyledTableCell>
                      <StyledTableCell>{row.lastname}</StyledTableCell>
                      <StyledTableCell>
                        <div style={{ display: 'flex' }}>
                          <div style={{ alignSelf: 'center', flex: 1 }}>
                            {row.number}
                          </div>
                          <div style={{ flex: 1 }}>
                            <IconButton onClick={() => editUser(row)} color="primary" aria-label="edit">
                              <EditIcon />
                            </IconButton>
                            <IconButton onClick={() => deleteOne(row.id)} color="secondary" aria-label="edit">
                              <DeleteIcon />
                            </IconButton>
                          </div>

                        </div>

                      </StyledTableCell>

                    </StyledTableRow>
                  ))}

                </TableBody>

              </Table>

            </Paper>
            <Tooltip style={{ alignSelf: 'flex-end', marginTop: '-30px' }} title="Add" aria-label="add">
              <Fab onClick={openPopup} color="primary" >
                <AddIcon />
              </Fab>
            </Tooltip>
          </div>
        )}

        <FormDialog isUpdate={isUpdate} preData={preData} open={isOpen} close={close} update={update}></FormDialog>
      </div>
    </React.Fragment>
  );
}

export default App;