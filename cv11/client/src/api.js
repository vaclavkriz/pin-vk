const baseUrl = require('./config.json').baseUri;

export const getUsers = () => {
    return fetch(`${baseUrl}/user/read.php`, {
        method: `GET`,
    });
}

export const addUser = (firstName, lastName, number) => {
    return fetch(`${baseUrl}/user/create.php`, {
        method: `POST`,
        body: JSON.stringify({
            firstname: firstName,
            lastname: lastName,
            number
        })
    });
}

export const deleteUser = (id) => {
    return fetch(`${baseUrl}/user/delete.php`, {
        method: `POST`,
        body: JSON.stringify({
            id
        })
    });
}

export const changeUser = (id, firstName, lastName, number) => {
    return fetch(`${baseUrl}/user/update.php`, {
        method: `POST`,
        body: JSON.stringify({
            id,
            firstname: firstName,
            lastname: lastName,
            number
        })
    });
}