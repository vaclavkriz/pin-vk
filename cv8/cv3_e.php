<?php session_start() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hodnoceni 2</title>
</head>
<body>
    <h1>Vyhodnocení numero dva</h1>
    <?php 
        if (isset($_POST["ok"]))
        {
            $sum = $_POST["otazka1"]+$_POST["otazka2"]+$_POST["otazka3"]+$_COOKIE["body"];
            setcookie("body");
            if($sum>=6)
            {
                echo "Dostal jsi ".$sum." bodů a tvoje známka je 1 CG";
            }
            else if($sum>=5)
            {
                echo "Dostal jsi ".$sum." bodů a tvoje známka je 2 CG";
            }
            else if ($sum>=4)
            {
                echo "Dostal jsi ".$sum." bodů a tvoje známka je 3. CG";
            }
            else
            {
                echo "Dostal jsi ".$sum." takže nejseš moc chytrej.";
            }
        }
        else
        {
            echo "Formulář není vyplněn. Nice try.";
        }
    ?>
</body>
</html>