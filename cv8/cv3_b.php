<?php session_start() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hodnoceni 1</title>
</head>
<body>
    <h1>Vyhodnocení numero jedna</h1>
    <?php 
        if (isset($_POST["ok"]))
        {
            $sum = $_POST["otazka1"]+$_POST["otazka2"]+$_POST["otazka3"];
            if($sum>=2)
            {
                echo "Uspěl jste!";
                echo '
                <h2>Vyberte si téma pro další kvíz!!!</h2>
                <ul>
                    <li><a href="cv3_c.php">Matematika</a></li>
                    <li><a href="cv3_d.php">Nematematika</a></li>
                </ul>
                ';
                setcookie("body", $sum);
            }
            else
            {
                echo "Nevyšlo to, zkus to příště líp.";
            }
        }
        else
        {
            echo "Formulář není vyplněn. Nice try.";
        }
    ?>
</body>
</html>