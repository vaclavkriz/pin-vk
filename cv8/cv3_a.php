<?php session_start() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kvízek 1</title>
</head>
<body>
    <h1>Kvíz numero jedna</h1>
    <form action="cv3_b.php" method="post">
        <fieldset>
            <legend>Kolik sousedů má Česká republika?</legend>
            <input type="radio" name="otazka1" value="0" />1<br>
            <input type="radio" name="otazka1" value="1" />4<br>
            <input type="radio" name="otazka1" value="0" />3<br>
        </fieldset>
        <fieldset>
            <legend>Jaký soused má nejdelší hranici s Českou republikou?</legend>
            <input type="radio" name="otazka2" value="1" />Německo<br>
            <input type="radio" name="otazka2" value="0" />Polsko<br>
            <input type="radio" name="otazka2" value="0" />Slovensko<br>
        </fieldset>
        <fieldset>
            <legend>Jaké je druhé největší město ČR?</legend>
            <input type="radio" name="otazka3" value="0" />Praha<br>
            <input type="radio" name="otazka3" value="1" />Brno<br>
            <input type="radio" name="otazka3" value="0" />Plzeň<br>
        </fieldset>
        <input type="submit" name="ok" value="Odeslat">
    </form>
</body>
</html>