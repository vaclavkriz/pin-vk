<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cv2</title>
</head>
<body>
    <h1>Seznam článků</h1>
    <ul>
        <li><a href="cv2_b.php">První článek</a></li>
        <li><a href="cv2_c.php">Druhý článek</a></li>
        <li><a href="cv2_d.php">Třetí článek</a></li>
    </ul>
    <h1>Historie navštívených článků</h1>
    <ul>
        <?php
            echo(isset($_COOKIE["text1"]) ? "<li>První článek již byl navštíven.</li>" : "");
            echo(isset($_COOKIE["text2"]) ? "<li>Druhý článek již byl navštíven.</li>" : "");
            echo(isset($_COOKIE["text3"]) ? "<li>Třetí článek již byl navštíven.</li>" : "");
        ?>
    </ul>
</body>
</html>