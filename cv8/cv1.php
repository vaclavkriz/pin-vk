<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cv1</title>
</head>
<body>
<?php
    if(!isset($_COOKIE['count']))
    {
        echo "<h1>Welcome! This is the first time you have visited this page.</h1>";
        $cookie = 1;
        setcookie('count',$cookie);
    }
    else
    {
        $cookie = ++$_COOKIE['count'];
        setcookie("count",$cookie);
        if ($_COOKIE['count'] == 5)
        {
            echo "<h1>You shall not go away!</h1>";
        }
        else if ($_COOKIE['count'] == 10)
        {
            echo "<h1>Still here?!</h1>";
        }
        else
        {
            echo "<h1>You have viewed this page " . $_COOKIE['count'] . " times.</h1>";
        }
    }
?>
</body>
</html>