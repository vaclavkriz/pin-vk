<?php session_start() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kvízek 2</title>
</head>
<body>
    <h1>Kvíz numero dva = Matematika</h1>
    <form action="cv3_e.php" method="post">
        <fieldset>
            <legend>1+1=?</legend>
            <input type="radio" name="otazka1" value="0" />1<br>
            <input type="radio" name="otazka1" value="1" />2<br>
            <input type="radio" name="otazka1" value="0" />0<br>
        </fieldset>
        <fieldset>
            <legend>1*1=?</legend>
            <input type="radio" name="otazka2" value="1" />1<br>
            <input type="radio" name="otazka2" value="0" />0<br>
            <input type="radio" name="otazka2" value="0" />2<br>
        </fieldset>
        <fieldset>
            <legend>1/1=?</legend>
            <input type="radio" name="otazka3" value="0" />0<br>
            <input type="radio" name="otazka3" value="1" />1<br>
            <input type="radio" name="otazka3" value="0" />2<br>
        </fieldset>
        <input type="submit" name="ok" value="Odeslat">
    </form>
</body>
</html>