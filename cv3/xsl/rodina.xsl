<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- 1. odstavce
<xsl:template match="osoba">
    <html>
        <body>
            <p><xsl:value-of select="@jmeno"/><br/><xsl:value-of select="vek"/></p>
        </body>
    </html>
</xsl:template>
-->

<!-- 2. usporadani podle veku
<xsl:template match="rodina">
<html>
    <body>
        <xsl:apply-templates select="osoba">
            <xsl:sort select="vek"/>
        </xsl:apply-templates>
    </body>
</html>
</xsl:template>

<xsl:template match="osoba">
<p>
    <xsl:value-of select="@jmeno"/>
    <br/>
    <xsl:value-of select="vek"/>
</p>
</xsl:template>
-->

<!-- 3. jmeno na radek
<xsl:template match="rodina">
<html>
    <body>
        <xsl:apply-templates select="osoba"/>
    </body>
</html>
</xsl:template>
<xsl:template match="osoba">
    <xsl:value-of select="@jmeno"/><br/>
</xsl:template>
-->

<!-- 4. tabulka
<xsl:template match="rodina">
<html>
    <body>
        <table border="solid">
            <tr>
                <th>Jmeno</th>
                <th>Vek</th>
                <th>Pohlavi</th>
            </tr>
            <xsl:apply-templates select="osoba"/>
        </table>
    </body>
</html>
</xsl:template>

<xsl:template match="osoba">
    <tr>
        <td><xsl:value-of select="@jmeno"/></td>
        <td><xsl:value-of select="vek"/></td>
        <td><xsl:value-of select="pohlavi"/></td>
    </tr>
</xsl:template>
-->
<!-- 5. rozdeleni podle veku  -->
<xsl:template match="rodina">
<html>
    <body>
        <xsl:apply-templates select='osoba'/>
    </body>
</html>
</xsl:template>

<xsl:template match="osoba">
    <tr>
        <font color="red"><td><xsl:value-of select="@jmeno"/></td></font>
    </tr>
</xsl:template>
<xsl:template match="osoba[vek>=40]">
    <tr>
        <font color="blue"><td><xsl:value-of select="@jmeno"/></td></font>
    </tr>
</xsl:template>

</xsl:stylesheet>

