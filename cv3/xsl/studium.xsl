<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- 1. odstavce
<xsl:template match="osoba">
    <html>
        <body>
            <p><xsl:value-of select="@jmeno"/><br/><xsl:value-of select="vek"/></p>
        </body>
    </html>
</xsl:template>
-->

<!-- 2. usporadani podle veku
<xsl:template match="rodina">
<html>
    <body>
        <xsl:apply-templates select="osoba">
            <xsl:sort select="vek"/>
        </xsl:apply-templates>
    </body>
</html>
</xsl:template>

<xsl:template match="osoba">
<p>
    <xsl:value-of select="@jmeno"/>
    <br/>
    <xsl:value-of select="vek"/>
</p>
</xsl:template>
-->

<!-- 3. jmeno na radek
<xsl:template match="rodina">
<html>
    <body>
        <xsl:apply-templates select="osoba"/>
    </body>
</html>
</xsl:template>
<xsl:template match="osoba">
    <xsl:value-of select="@jmeno"/><br/>
</xsl:template>
-->

<!-- 4. tabulka
<xsl:template match="rodina">
<html>
    <body>
        <table border="solid">
            <tr>
                <th>Jmeno</th>
                <th>Vek</th>
                <th>Pohlavi</th>
            </tr>
            <xsl:apply-templates select="osoba"/>
        </table>
    </body>
</html>
</xsl:template>

<xsl:template match="osoba">
    <tr>
        <td><xsl:value-of select="@jmeno"/></td>
        <td><xsl:value-of select="vek"/></td>
        <td><xsl:value-of select="pohlavi"/></td>
    </tr>
</xsl:template>
-->

<!-- 1. Cislovany seznam predmetu
<xsl:template match="studium">
<html>
    <body>
        <ol>
            <xsl:apply-templates select="rok/semestr//predmet"/><br/>
        </ol>
    </body>
</html>
</xsl:template>

<xsl:template match="predmet">
    <li><xsl:value-of select="nazev"/></li>
</xsl:template>
-->

<!-- 2. Kredity > 2
<xsl:template match="studium">
<html>
    <body>
        <h1>Predmety</h1>
        <xsl:apply-templates select="//semestr"/>
    </body>
</html>
</xsl:template>

<xsl:template match="semestr">
<h2><xsl:value-of select="@semid"/></h2>
<table border="solid" width="400px" >
    <tr>
        <th>Nazev</th>
        <th>Kredity</th>
    </tr>
    <xsl:apply-templates select="predmet[kredity>2]"/>
</table>
</xsl:template>

<xsl:template match='predmet'>
<tr>
    <td><xsl:value-of select="nazev"/></td>
    <td><xsl:value-of select="kredity"/></td>
</tr>
</xsl:template>
-->

<!-- 3. Premdety serazene podle kreditu
<xsl:template match="studium">
<html>
    <body>
        <xsl:apply-templates select="//semestr[@semid='r2l']"/>
    </body>
</html>
</xsl:template>

<xsl:template match='semestr'>
<table border="solid">
    <tr>
        <th>Nazev</th>
        <th>Kredity</th>
    </tr>  
    <xsl:apply-templates select='predmet'>
        <xsl:sort select='kredity' data-type='number' order="descending"/>
    </xsl:apply-templates>
</table>
Celkem kreditnich bodu: <xsl:value-of select="sum(predmet/kredity/text())"/>
</xsl:template>

<xsl:template match="predmet">
<tr>
    <td><xsl:value-of select="nazev"/></td>
    <td><xsl:value-of select='kredity'/></td>
</tr>
</xsl:template>
-->

<!-- 4. Semestry serazene podle kreditu
<xsl:template match="studium">
<html>
    <body>
        <ul>
            <xsl:apply-templates select="//semestr">
                <xsl:sort select="sum(predmet/kredity/text())" data-type='number' order='descending'/>
            </xsl:apply-templates>
        </ul>
    </body>
</html>
</xsl:template>

<xsl:template match='semestr'>
<li><xsl:value-of select="@semid"/> - <xsl:value-of select="sum(predmet/kredity/text())"/></li>
</xsl:template>
-->

<!-- 5. Predmety podle katedry -->
<xsl:template match="studium">
<html>
    <body>
        <xsl:apply-templates select="//semestr[@semid='r1z']"/>
    </body>
</html>
</xsl:template>

<xsl:template match="semestr">
        <ul>
            <xsl:apply-templates select="predmet"/>
        </ul>
</xsl:template>

<xsl:template match="predmet[katedra='KI']">
<li style="background:purple"><xsl:value-of select="nazev"/></li>
</xsl:template>

<xsl:template match="predmet[katedra='CJP']">
<li style="background:blue"><xsl:value-of select="nazev"/></li>
</xsl:template>

<xsl:template match="predmet[katedra='KMA']">
<li style="background:green"><xsl:value-of select="nazev"/></li>
</xsl:template>

<xsl:template match="predmet[katedra='KFY']">
<li style="background:hotpink"><xsl:value-of select="nazev"/></li>
</xsl:template>

</xsl:stylesheet>

