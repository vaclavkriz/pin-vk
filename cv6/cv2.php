<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <div class="formwrapper">
        <form class="formular" action="#" method="POST">
            <label for="name">Jméno</label>
            <input type="text" name="name" />
            <label for="gender">Pohlaví</label>
            <select name="gender">
                <option value="muž">muž</option>
                <option value="žena">žena</option>
                <option value="jiné">jiné</option>
            </select>
            <label for="title">Titul</label>
            <input type="text" name="title" />
            <label for="date">Datum</label>
            <input type="date" name="date" />
            <input class="submit-btn" type="submit" name="ok" value="Odeslat" />
        </form>
    </div>
    <?php

    $jmeno = "";
    $gender  = "";
    $title = "";
    $date  = "";
    if(isset($_POST['ok'])){
        $jmeno = $_POST['name'];
        $gender = $_POST['gender'];
        $title = $_POST['title'];
        $date = $_POST['date'];    
        $text = pozvanka($title, $jmeno, $date, $gender);
        echo "<span>$text</span>";
    }
    ?>
</body>
<?php
function pozvanka($title, $jmeno, $datum, $pohlavi){
        $gender = "";
        if($pohlavi === 'žena'){
            $gender = 'paní';
        }
        else {
    
            $gender = 'pane';
        }
        $text = "Dobrý den $gender $title $jmeno. Dnes je datum: $datum";
        return $text;
    }

?>


<style>
    .formwrapper {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .formular {
        display: flex;
        flex-direction: column;
        width: 50%;
    }

    input,
    select {
        height: 50px;
        font-size: 30px;
        border-radius: 10px;
        border: 2px solid #ccc;
    }

    .submit-btn {
        margin-top: 10px;
    }
</style>

</html>