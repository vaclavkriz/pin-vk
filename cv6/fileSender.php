<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>File sender</title>
</head>

<body>
    <div class="containerc">
        <h1>Nahrávačka souborů</h1>
        <form style="width:50%;" enctype="multipart/form-data" action="#" method="POST">
            <div style="width:100%;" class="form-group">
                <label style="width:100%;" for="file-upload" class="custom-file-upload">
                    <img id="upImg" class="upImg" src="upload.png" />
                </label>
                <input onchange="loadFile(event)" accept="image/gif, image/jpeg, image/png" name="fileToSend" id="file-upload" type="file" />
                <button style="width:100%;" type="submit" name="ok" class="btn btn-success btn-lg">Odeslat</button>

            </div>
        </form>
        <?php
        if (isset($_POST['ok']) && (string) $_FILES['fileToSend']['error'] == "0") {
            $uploads_dir = 'uploads';
            $upFile = $_FILES['fileToSend'];
            $name = basename($upFile['name']);
            $tempSuff = 0;
            $tempName = $name;
            while (file_exists("$uploads_dir/$tempName")) {
                $tempName = $tempSuff . $name;
                $tempSuff++;
            }
            $name = $tempName;
            move_uploaded_file($upFile['tmp_name'], "$uploads_dir/$name");
            echo "
            <h2 style='text-align:center;'>Údaje o odeslaném souboru</h2><table class='table  table-striped'>
            <thead  class='thead-dark'>
                <tr>
                    <th scope='col'>název</th>
                    <th scope='col'>typ</th>
                    <th scope='col'>cesta</th>
                    <th scope='col'>velikost</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>$upFile[name]</td>
                    <td>$upFile[type]</td>
                    <td>$upFile[tmp_name]</td>
                    <td>$upFile[size]</td>
                </tr>
            </tbody>
            </table>
";
        } else if (isset($_POST['ok']) && !(string) $_FILES['fileToSend']['error'] == "0") {
            echo '<h2 style="text-align:center;" class="text-danger">Byl vybrán neplatný soubor</h2>';
        }
        $directory = "uploads";
        $images = glob($directory . "/*.{jpg,png,gif}", GLOB_BRACE);
        $dom = '<h2 style="text-align:center;">Galerie odeslaných souborů</h2><div class="gallery">';
        foreach ($images as $image) {
            $dom .= '<img class="gal-img" src="' . $image . '" />';
        }
        $dom .= '</div>';
        echo $dom;
        ?>
        <script>
            var loadFile = function(event) {
                var image = document.getElementById('upImg');
                image.src = URL.createObjectURL(event.target.files[0]);
            };
        </script>
    </div>

</body>
<style>
    .containerc {
        display: flex;
        justify-content: center;
        flex-direction: column;
        align-items: center;
        margin-top: 20px;
    }

    .table {
        width: 95%;
        margin: 0px 10px 1rem 10px;
    }

    @media only screen and (min-width: 900px) {
        .table {
            width: 50%;

        }
    }

    input[type="file"] {
        display: none;
    }

    .custom-file-upload {
        display: flex;
        justify-content: center;
        border: 1px solid #ccc;
        border-radius: .3rem;
        align-items: center;
        padding: 6px 12px;
        width: 100%;
        cursor: pointer;
        overflow: hidden;
        max-height: 50vh;
    }

    .upImg {
        transition: transform .4s;
        max-width: 500px;
        width: 100%;
    }

    .upImg:hover {
        transform: scale(1.3);
    }

    .gallery {
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        margin: 0px 20px 20px 20px;
        border: 1px solid black;
        border-radius: 20px;
        box-shadow: 5px 5px 10px;
        background: whitesmoke;
    }

    .gal-img {
        max-width: 300px;
    }


    }
</style>

</html>