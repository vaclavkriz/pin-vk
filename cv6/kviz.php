<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Kvíz</title>
</head>

<body>
    <form action="#" method="POST">
        <h1 style="font-size:80px;">Quiz P&V</h1>
        <div class="form-wrapper">
            <div>
                <h2>What year saw the release of H. G. Wells' "The Time Machine"?</h2>
                <div class="divider"></div>
                <div class="answer">
                    <label>1832</label>
                    <input type="radio" name="q0" value="q0_0">
                </div>
                <div class="answer">
                    <label>1942</label>
                    <input type="radio" name="q0" value="q0_1">
                </div>
                <div class="answer">
                    <label>1895</label>
                    <input type="radio" name="q0" value="q0_2">
                </div>
                <div class="points"> <span>3body</span></div>

            </div>

        </div>
        <div class="form-wrapper">
            <div>
                <h2>What newspaper does Superman work for?</h2>
                <div class="divider"></div>
                <div class="answer">
                    <label>The Sun</label>
                    <input type="radio" name="q1" value="q1_0">
                </div>
                <div class="answer">
                    <label>The Daily Planet</label>
                    <input type="radio" name="q1" value="q1_1">
                </div>
                <div class="answer">
                    <label>The Extravaganza</label>
                    <input type="radio" name="q1" value="q1_2">
                </div>
                <div class="points"> <span>4body</span></div>
            </div>

        </div>
        <div class="form-wrapper">
            <div>
                <h2>Which character killed Albus Dumbledore?</h2>
                <div class="divider"></div>
                <div class="answer">
                    <label>Severus Snape</label>
                    <input type="radio" name="q2" value="q2_0">
                </div>
                <div class="answer">
                    <label>Draco Malfoy</label>
                    <input type="radio" name="q2" value="q2_1">
                </div>
                <div class="answer">
                    <label>Harry Potter</label>
                    <input type="radio" name="q2" value="q2_2">
                </div>
                <div class="points"> <span>5bodů</span></div>
            </div>
        </div>
        <div class="sbm-btn">
            <button type="submit" name="sendData" class="btn btn-lg btn-success">Odeslat</button>
        </div>
    </form>
    <div style="display:flex;justify-content:center;">
        <?php
        $correctAnswers = [
            "q0" => "q0_2",
            "q1" => "q1_1",
            "q2" => "q2_0"
        ];
        $points = [
            "q0" => 3,
            "q1" => 4,
            "q2" => 5
        ];
        $point = 0;
        if (isset($_POST['sendData'])) {
            if (isset($_POST["q0"]) &&  $_POST["q0"] == $correctAnswers["q0"]) {
                $point = $point + $points["q0"];
            }
            if (isset($_POST["q1"]) &&  $_POST["q1"] == $correctAnswers["q1"]) {
                $point = $point + $points["q1"];
            }
            if (isset($_POST["q2"]) &&  $_POST["q2"] == $correctAnswers["q2"]) {
                $point = $point + $points["q2"];
            }
            if ($point > 6) {
                echo "<h1 style='color:lime;'>You have successfully passed the test with $point points.</h1>";
            } else {
                echo "<h1 style='color:red;'>You have failed the test with $point points.</h1>";
            }
        }

        ?>
    </div>
</body>
<style>
    form {
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
    }

    .divider {
        border-bottom: 1px solid #ccc;
    }

    .form-wrapper {
        width: 40%;
        border: 2px solid black;
        text-align: center;
        border-radius: 10px;
        margin-top: 30px;
        box-shadow: 5px 5px 10px;

    }

    .answer {
        font-size: 25px;
    }

    .points {
        color: red;
        display: flex;
        justify-content: flex-end;
        font-weight: bold;
        margin-right: 1%;
    }

    input[type="radio"] {
        margin-left: 5px;
        transform: scale(1.8);
    }

    .sbm-btn {
        margin-top: 30px;
        width: 40%;
        display: flex;
    }

    .btn {
        width: 100%;

        border-radius: 10px;
        font-size: 30px;
        font-weight: bolder;
    }
</style>

</html>