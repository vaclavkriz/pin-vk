<?php
require_once('process.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="header.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.0/css/bootstrap.min.css" integrity="sha384-SI27wrMjH3ZZ89r4o+fGIJtnzkAnFs3E4qz9DIYioCQ5l9Rd/7UAa8DHcaL8jkWt" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
    <title>SimpleXML</title>
</head>
<body>

<div class="container">
<h1 class="nadpis">Předměty</h1>
<form class="opt-form" action="#" method="POST"> 
<span>Vyberte ročník</span>
<select name="rocnik">
  <option value="">Všechny</option>
  <option value="r1">1.ročník</option>
  <option value="r2">2.ročník</option>
</select>
<span>Vyberte semestr</span>
<select name="semestr">
  <option value="">Všechny</option>
  <option value="z">Zimní semestr</option>
  <option value="l">Letní semestr</option>
  
</select>
<input name="ok" type="submit" value="Odeslat" class="btn btn-success btn-send"></input>
</form>
<div class="outta-table">
<div class="table-courses">
<?php
 
if(isset($_POST['ok'])){
    $st = new Studium();
    $html = $st->printTable();
    echo $html;
}
?>
  <!-- Content here -->
</div>
  </div>
</div>
</div>
</body>
</html>