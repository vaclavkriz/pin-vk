<?php

class Studium {
private $rocnik;
private $semestr;
private $xml;
public function __construct(){
    $this->rocnik = $_POST['rocnik'];
    $this->semestr = $_POST['semestr'];
    $this->xml = simplexml_load_file('studium.xml');
}

private function getPredmety(){
    //echo $this->rocnik. $this->semestr;
   // var_dump( $this->xml);
     if(empty($this->rocnik) && !empty($this->semestr)){
      $p = "//semestr[@semid='$this->semestr']/predmet";
    }
    else if(empty($this->semestr) && !empty($this->rocnik)){
      $p = "//rok[@rocnik='$this->rocnik']/semestr/predmet";
    }
    else if(empty($this->semestr) && empty($rocnik)){
      $p = "//predmet";
    }
    else {
      $p = "//rok[@rocnik='$this->rocnik']/semestr[@semid='$this->semestr']/predmet";
    }
     $predmety = $this->xml->xpath($p);
    return $predmety;
}

public function printTable(){
    $predmety = $this->getPredmety();
    $html = "<table class='table table-striped'><thead>
    <tr>
      <th scope='col'>#</th>
      <th scope='col'>id</th>
      <th scope='col'>nazev</th>
      <th scope='col'>garant</th>
      <th scope='col'>typ zakočení</th>
      <th scope='col'>kredity</th>
      <th scope='col'>znamka</th>
      <th scope='col'>úspěšně zakončeno</th>
      <th scope='col'>volitelny</th>
    </tr></thead><tbody>";

    //<th scope='row'>1</th>
    foreach ($predmety as $i=>$key) {
      $id = $key->attributes()->id;
      $name = $key->nazev;
      $garant = $key->garant;
      $tz = $key->typ_zakonceni;
      $kredity = $key->kredity;
      $znamka= $key->znamka;
      $uz = $key->uspesne_zakoncen? "Ano": "Ne";
      $volitelny = $key->volitelny? "Ano":"Ne";
      
      $html .= "<tr>
      <td scope='row'>$i</td>
      <td>$id</td>
      <td>$name</td>
      <td>$garant</td>
      <td>$tz</td>
      <td>$kredity</td>
      <td>$znamka</td>
      <td>$uz</td> 
      <td>$volitelny</td>
    </tr>";   
    }
    $html.="  </tbody>
    </table>";
    return $html;
}
}

