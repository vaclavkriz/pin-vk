<?php
require_once('./library.php');
$library = new Library();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>XML DOM</title>
</head>
<body>
<div class="container">
<h1>Vyhledávání knih podle názvu</h1>
<div class="form-books">
<form method="POST" action="">
        <div class="input-wrapper">
            <label>Vyhledejte knihu podle názvu:</label>
            <input name="nazev" class="search-input"  />
        </div>
        <button name="ok" type="submit" class="btn btn-success btn-sm btn-search">Vyhledat</button>
</form>
</div>
<?php
if(isset($_POST['ok'])){
    $nazev = $_POST['nazev'];
    $books = $library->findBooksByName($nazev);
    $library->printTags($books, 'autor', 'nazev', $nazev, 'find1');
}
?>
<h1>Vyhledávání knih podle autora</h1>
<div class="form-books">
<form method="POST" action="">
        <div class="input-wrapper">
            <label>Vyhledejte knihu podle autora:</label>
            <input name="autor" class="search-input"  />
        </div>
        <button name="ok2" type="submit" class="btn btn-success btn-sm btn-search">Vyhledat</button>
</form>
</div>
<?php
if(isset($_POST['ok2'])){
    $books = $library->findBooksByAutor();
    $library->printTags($books, 'autor', 'nazev', 'find2');
}
?>
<h1>Anglické knihy</h1>
    <?php
       $books = $library->getBooksByLanguage('en');
       $library->printTags($books,'autor','nazev','cena');
    ?>
<h1>Levnější knihy než 250</h1>
    <?php 
        $books = $library->getBooksByPriceCheaper(250);
        $library->printTags($books, 'autor', 'nazev', 'vydavatelstvi');
    ?>
<h1>Sekce se zastaralými knihami < 2000</h1>
    <?php 
        $sections = $library->getOldSectionByYear(2000);
        $library->printAttributeValues($sections,'nazev','sekce');
    ?>
<div>
</body>
</html>
