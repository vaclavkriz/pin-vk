<?php

class Library {

    private $dom;
    private $xpath;

    public function __construct(){
        $this->dom = new DOMDocument();
        $this->dom->load("knihovna.xml");
        $this->xpath = new DOMXPath($this->dom);
    }
    public function getBooksByLanguage($lan){
    $knihy = $this->xpath->query("//kniha[@jazyk='$lan']"); 
    return $knihy;
 }

    public function getBooksByPriceCheaper($price){
        $knihy = $this->xpath->query("//kniha");
        $knihyRes = array();
        foreach ($knihy as $kniha) {
            if($kniha->getElementsByTagName("cena")->item(0)->nodeValue < $price){
                array_push($knihyRes, $kniha);
            }
        }
        return $knihyRes;
    }

    public function getOldSectionByYear($year){

        $sekce = $this->xpath->query("//sekce");
        $oldSekce = array();
        foreach ($sekce as $s) {
            $knihy = $s->getElementsByTagName("kniha");
            $isOld = false;
            foreach($knihy as $kniha){
                if($kniha->getElementsByTagName("rok")->item(0)->nodeValue < $year){
                    $isOld = true;
                    break;
                }
            }
            if($isOld){
                array_push($oldSekce, $s);
            }
        }
        return $oldSekce;
    }
    public function findBooksByName(string $nazev){
        if(isset($nazev)){
            $books= $this->xpath->query('//kniha');
                if(empty($nazev)){
                    return  $books;
                }
           $booksRes = array();
           foreach($books as $book)
           {    
               $tmp = $book->getElementsByTagName("nazev")->item(0)->nodeValue;
                if (strpos(strtolower($tmp), strtolower($nazev)) !== false)
                {
                    array_push($booksRes, $book);
                }   
        }
      }
      return $booksRes;
    }
    public function findBooksByAutor(){
        $nazev = $_POST['autor'];
        if(isset($nazev)){
           $books= $this->xpath->query('//kniha');
           if(empty($nazev)){
            return  $books;
        }
           $booksRes = array();
           foreach($books as $book){    
                if (strpos( strtolower($book->getElementsByTagName("autor")->item(0)->nodeValue), strtolower($nazev)) !== false)
                {
                    array_push($booksRes, $book);
                }
        }
      }
      return $booksRes;
    }
    public function printTags($xml,...$args){
        $type = end($args);
        if ($type == 'find1'){
            $replace = $args[2];      
            array_pop($args);
            array_pop($args);
        }
        if ($type == 'find2'){
            array_pop($args);
        }         
        $booksc = array();
        $html = "<table class='table table-hover'><thead><tr>";
        foreach($args as $arg){
            $html .= "<th>$arg</th>";
        }
        $html .= "</tr></thead><tbody>";
        foreach($xml as $x){
            $html .= "<tr>";
            if ($type != 'find1'){
                foreach($args as $arg){
                $html .= "<td>".$x->getElementsByTagName("$arg")->item(0)->nodeValue . "</td>";
                }
            } 
            else {
                foreach($args as $arg){
                    if ($arg == 'nazev')
                    {
                        $html .= "<td>".str_replace($replace,"<span style='background-color:yellow;'>$replace</span>",$x->getElementsByTagName("$arg")->item(0)->nodeValue). "</td>";
                    }
                    else
                    {
                        $html .= "<td>".$x->getElementsByTagName("$arg")->item(0)->nodeValue . "</td>";
                    }
                }
            }
            
            $html .= "</tr>";
        }
        $html .= "</tbody><table>";
        echo $html;
    }
    public function printAttributeValues($xml, $attr,...$args){
        $booksc = array();
        $html = "<table class='table table-hover'><thead><tr>";
        foreach($args as $arg){
            $html .= "<th>$arg</th>";
        }
        $html .= "</tr></thead><tbody>";
        foreach($xml as $x){
            $html .= "<tr>";
            foreach($args as $arg){
                $html .= "<td>".$x->getAttribute($attr) . "</td>";
            }
            $html .= "</tr>";
        }
        $html .= "</tbody><table>";
        echo $html;
    }
}
