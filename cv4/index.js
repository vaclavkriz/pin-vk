

const getHighestNumber = () => {
    const listNodes = document.querySelector('#list');
    const listItems = listNodes.children;
    const arrayNodes = Array.from(listItems);
    let biggest;
    arrayNodes.forEach((a, i) => {
        if (!biggest) biggest = a;
        else if (a.textContent < biggest) {
            biggest = a;
        }
    })
    biggest.style.color = 'red';
}

const setTableColor = () => {
    document.querySelectorAll('#table tr')[1].style.background = "green";
    document.querySelectorAll('#table tr')[2].style.background = "yellow";
}

const changeColor = () => {
    const trs = document.querySelectorAll('#table2 tr');
    trs.forEach((t, i) => {
        if (i !== 0) {
            if (i % 2 === 0) {
                t.style.background = 'cyan'
            }
            else {
                t.style.background = 'lime'
            }
        }
    })
}


const findBiggestTh = () => {
    const listNodes = document.querySelectorAll('#table3 td');
    const arrayNodes = Array.from(listNodes);
    console.log(arrayNodes)
    let biggest;
    arrayNodes.forEach((a, i) => {
        if (!biggest) biggest = a;
        else if (a.textContent < biggest) {
            biggest = a;
        }
    })
    biggest.style.background = 'yellow';
}