<?php

class Student {
    
    public $sn;
    public $lastName;
    public $year;
    public $credits;
    
    public function __construct(){
        $this->sn = isset($_POST['sn']) ? $_POST['sn'] : null;
        $this->lastName = isset($_POST['lastname']) ? $_POST['lastname'] : null;
        $this->year = isset($_POST['year']) ? $_POST['year'] : null;
        $this->credits = isset($_POST['credits']) ? $_POST['credits']: null;
    }

    public function changeYear($_year){
        if($this->year){
            $this->year = $_year;
        }
    }

    public function addCredits($_credits){
        if($this->credits){
            $this->credits += $_credits;
        }
    }
}