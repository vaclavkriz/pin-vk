<?php
require_once('Student.php');
class Studenti {
    private $studenti;
    private $file;

    public function __construct(){
        $this->studenti = array(); 
        $this->file = json_decode(file_get_contents('./dir/students.json'), true);

    }

    public function addStudent($student){
        $json = $this->file;
        $json[$student->sn]['lastName'] = $student->lastName;  
        $json[$student->sn]['year'] = $student->year;  
        $json[$student->sn]['credits'] = $student->credits; 


        $fp = fopen('./dir/students.json', 'w');
        fwrite($fp, json_encode($json));
        fclose($fp);
    }

    public function displayStudents(){
        
        $temp = "";
        foreach ($this->file as $sn => $stObj) {
            $ln = $stObj['lastName'];
            $yr = $stObj['year'];
            $cr = $stObj['credits'];

            $temp .=  "<tr>
            <td>$sn</td>
            <td>$ln</td>                        
            <td>$yr</td>
            <td>$cr</td>
            <td><a href='index.php?sn=$sn&ln=$ln&yr=$yr&cr=$cr'>✎</a></td>
        </tr>";
        }
        return $temp;
    }
}

