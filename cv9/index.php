<?php
require_once('./class/Student.php');
require_once('./class/Studenti.php');


$students = new Studenti();
if (isset($_POST['sn']) && $_POST['sn']) {
    $student = new Student();
    $students->addStudent($student);
    $students = new Studenti();
}

$sn = "";
$ln = "";
$yr = "";
$cr = "";
if (isset($_GET['sn'])) {
    $sn = $_GET['sn'];
    $ln = $_GET['ln'];
    $yr = $_GET['yr'];
    $cr = $_GET['cr'];
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cv1</title>
</head>

<body>
    <?php

    $disabled = $sn ? true : false;

    if ($disabled) {

        echo "
    <form action='index.php' method='post'>
    <div>
        <span>Student number</span>
        <input style='background: #ccc;' name='sn' type='text' readonly value='$sn' />
    </div>
    <div>
        <span>Last name</span>
        <input name='lastname'  value='$ln'  type='text' />
    </div>
    <div>
        <span>Year</span>
        <input name='year'  value='$yr' type='text' />
    </div>
    <div>
        <span>Credits</span>
        <input name='credits'  value='$cr' type='text' />
    </div>
    <button type='submit'>Odeslat</button>
    </form>
    
    ";
    } else {
        echo "
    <form action='index.php' method='post'>
    <div>
        <span>Student number</span>
        <input name='sn' type='text' value='$sn' />
    </div>
    <div>
        <span>Last name</span>
        <input name='lastname' value='$ln' type='text' />
    </div>
    <div>
        <span>Year</span>
        <input name='year' value='$yr' type='text' />
    </div>
    <div>
        <span>Credits</span>
        <input name='credits' value='$cr' type='text' />
    </div>
    <button type='submit'>Odeslat</button>
    </form>
    
    ";
    }

    ?>

    <table>
        <thead>
            <th>Student number</th>
            <th>Last name</th>
            <th>Year</th>
            <th>Credits</th>
            <th></th>
        </thead>
        <tbody>
            <?php

            echo $students->displayStudents();
            ?>
        </tbody>
    </table>

</body>
<style>
    table {
        margin-top: 10px;
    }

    table,
    th,
    td {
        border: 1px solid black;
    }

    div {
        display: flex;
        flex-direction: column;
    }

    body {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }

    form {
        width: 50%;
    }

    input {
        border: 2px solid #ccc;
        border-radius: 10px;
        height: 20px;
    }

    *:focus {
        outline: none;
    }

    button {
        width: 100%;
        margin-top: 10px;
        border: 2px solid #ccc;
        border-radius: 10px;
        height: 20px;
    }
</style>

</html>