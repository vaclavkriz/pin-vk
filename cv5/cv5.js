/*
*  1 seznam predmetu pro kazdy semestr kr > 4
* 2 seznam semestru, zvyraznit semestr s nejvyssim poctem kreditu
* 3 seznam  - 1 predmet z kazdeho semestru, zmenit pozadi predmetu s nejvyssim poctem kreditu
 * 
*/

(() => {

  ///OPEN RODINA.XML

  const requestRodina = new XMLHttpRequest();
  requestRodina.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      processRodina(this);
    }
  };
  requestRodina.open("GET", "rodina.xml", true);
  requestRodina.send();

  ///OPEN STUDIUM.XML

  const requestStudium = new XMLHttpRequest();
  requestStudium.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      processStudium(this);
    }
  };
  requestStudium.open("GET", "studium.xml", true);
  requestStudium.send();



  const cviceni0 = a => {

    const osobyNad40 = Array.from(a)
      .filter(osoba => osoba.querySelector('vek').innerHTML > 40)
      .map(o => `<li>${o.attributes[2].nodeValue}</li>`)
    document.querySelector('#rodina').innerHTML = `<ul>${osobyNad40}</ul>`
  }

  const cviceni1 = semestry => {
    const predmetyHTML = Array.from(semestry).map(s => {
      const predmety = s.querySelectorAll('predmet');
      const filteredPredmetyHTML = Array.from(predmety)
        .filter(p => {
          return p.querySelector('kredity').innerHTML > 4
        })
        .map(predmet => {
          return `<li>${predmet.querySelector('nazev').innerHTML}</li>`
        });
      return `<ul>${filteredPredmetyHTML}</ul>`
    })

    document.querySelector('#predmetyNad4').innerHTML = predmetyHTML;
  }

  const cviceni2 = semestry => {
    const semestrKredityObjekt = Array.from(semestry).map((s, i) => {
      const predmety = s.querySelectorAll('predmet');
      const semestrReduce = Array.from(predmety).reduce((sma, smb, i) => {
        return sma + +smb.querySelector('kredity').innerHTML;
      }, 0)
      return { semestr: s, kredity: semestrReduce, isBiggest: false }
    })
    let biggestKredit;
    let biggestIndex;
    Object.keys(semestrKredityObjekt).forEach(a => {
      if (!biggestKredit && !biggestIndex) {
        biggestKredit = semestrKredityObjekt[a].kredity;
        biggestIndex = a;
      }
      else if (semestrKredityObjekt[a].kredity > biggestKredit) {
        biggestKredit = semestrKredityObjekt[a].kredity;
        biggestIndex = a;
      }
    })
    semestrKredityObjekt[biggestIndex].isBiggest = true;
    const semestryHTML = semestrKredityObjekt.map(semestr => {
      return `<li style="${semestr.isBiggest ? 'color:red' : null}">${semestr.semestr.attributes[0].nodeValue} - ${semestr.kredity}</li>`
    })
    document.querySelector('#semestry').innerHTML = `<ul>${semestryHTML}</ul>`;
  }

  const cviceni3 = semestry => {
    const semestrFirstPredmet = Array.from(semestry).map(s => {
      const predmet = s.querySelectorAll('predmet')[Math.floor(Math.random() * s.querySelectorAll('predmet').length)];
      const kredity = predmet.querySelector('kredity').innerHTML;
      return {
        predmet, kredity, isBiggest: false
      }
    })
    let biggestKredit2;
    let biggestIndex2;
    Object.keys(semestrFirstPredmet).forEach((a, i) => {

      if (!biggestKredit2 && !biggestIndex2) {
        biggestKredit2 = semestrFirstPredmet[a].kredity;
        biggestIndex2 = a;
      }
      else if (semestrFirstPredmet[a].kredity > biggestKredit2) {
        biggestKredit2 = semestrFirstPredmet[a].kredity;
        biggestIndex2 = a;
      }
    })
    semestrFirstPredmet[biggestIndex2].isBiggest = true;
    const predmety2HTML = semestrFirstPredmet.map(semestr => {
      return `<li style="${semestr.isBiggest ? 'background:red' : null}">${semestr.predmet.querySelector('nazev').innerHTML} - ${semestr.kredity}</li>`
    })
    document.querySelector('#predmety').innerHTML = `<ul>${predmety2HTML}</ul>`;
  }


  //RODINA.XML
  const processRodina = xml => {
    const xmlDoc = xml.responseXML;
    const x = xmlDoc.documentElement;
    const a = x.querySelectorAll('osoba')
    //cviceni0
    cviceni0(a);
  }

  //STUDIUM.XML
  const processStudium = xml => {
    const xmlDoc = xml.responseXML;
    const x = xmlDoc.documentElement;

    const semestry = x.querySelectorAll('semestr')
    //cv1
    cviceni1(semestry);
    //cv2
    cviceni2(semestry);
    //cv3
    cviceni3(semestry);
  }

})()

